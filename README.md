# README #

### What is this repository for? ###

The ExAMS packages is born after assembling together several python little programs that were initially used to analyse one dataset. The goal is to provide a package that could be used for analogue analysis of other datasets.

This package will be progressively extended with further analysis and visualisation facilities.

### How do I get set up? ###

* Downloading and installing ExAMS

Use git to download the ExAMS package:

```
#!shell

git clone https://username@bitbucket.org/ExAMS/exams.git

```
After the download is complete change into the ExAMS directory and run the setup script:

```
#!shell

cd ExAMS
python setup.py install --user

```

If you are working in a C shell, remember to type a rehash command in order to refresh the PATH variable that will now contain the path to the ExAMS executable:

```
#!shell

rehash
```

Verify the installation by typing:

```
#!shell

ExAMS -h
```

or simply start just with:

```
#!shell

ExAMS -h
```

* Dependencies

Before installing the ExAMS package you will need to install the following packages: *numpy* (minimum version 1.0.3), *MDAnalysis* and *argh*. *MDAnalysis* will most likely require to have the *setuptools* package installed. Moreover, since it is a package that integrates several C++ modules inside it, it will require to have *gcc* and *g++* compilers of a version possibly greater than 4.3.

Therefore type in order:

```
#!shell

pip install numpy --user
pip install setuptools --user
pip install MDAnalysis --user
```

* How to run tests

In the test folder inside the ExAMS folder, you will find 3 shell scripts, each of which containing the commands to run ExAMS selecting one of the options of the ExAMS package. The data (necessary input files) for the *option3.sh* script is provided while the data for the other two scripts is assumed to be stored in the workstations (of CBS). Please, feel free to modify the scripts and produce your own scripts using ExAMS according to your needs.

### Who do I talk to? ###

* ardita.shkurti@gmail.com (Repo owner/admin)
* charles.laughton@nottingham.ac.uk